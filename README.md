# Pack, Ship, Deliver! #

*You decide to start up a business called Jungle in the bustling city of Seattle, Washington that will focus on one thing: online product distribution. You want to provide an online website where people from all over the country (and eventually the world!) will buy and sell via your warehouse in Seattle. Sellers will provide the merchandise, and your business will handle the stocking, packaging, and shipping. Although you’d like to hire workers to do all of the packing, shipping, and handling of the merchandise, you know that your start up can only afford to employ one person: yourself.*

*Now you have the daunting task of running Jungle’s Warehouse. Will you succeed?*

### Gameplay Description ###

Pack, Ship, Deliver is primarily a Simulation / Management game in which you run around the warehouse gathering the correct products, boxes, packing materials, and shipping labels in order to fulfill customer orders. If you use the incorrect products/boxes/packing materials/shipping labels, the customer will be unhappy and will not leave your website a favorable review. As your average customer review rises the more orders you will receive, the more sellers will trust their products with you, and the more you can charge for services rendered!

Like all simulation games, you can upgrade your warehouse to:
* Hold more items
* Allow more items per order
* Allow heavier items per order (and thus bigger boxes!)
* Have fancy conveyor belts
* Use a Smart Self Balancing Scooter to get around
* Offer cheaper shipping
* Offer a members-only free shipping system called Jungle Select
* Raise the prices of products

Those are just a few of the upgrades that will be available. **However, the current iteration of the game does not have upgrades available!**

Additionally, the game is in first person and rendered in 3D.
 
Get ready to Pack, Ship, and Deliver!


### Game Technologies ###

The game will be created using Unreal Engine 4.9 and C++. Many of the game components will use blueprints (Unreal Engine’s visual programming language), but some will make use of Unreal’s C++ classes. 

The user interface will be constructed using BLUI. This is an open source Unreal Engine plugin that: “allows easy interaction with the Chromium Embedded Framework. It provides a simple Material Instance and input functions to help streamline the rendering of rich HTML interfaces,” ([BLUI Github page](https://github.com/AaronShea/BLUI)). In other words, this will create HTML5 + JavaScript enabled pages to use as UI components in game. 


### Screenshots ###

![pack-ship-deliver.png](https://bitbucket.org/repo/XEyook/images/1038410787-pack-ship-deliver.png)