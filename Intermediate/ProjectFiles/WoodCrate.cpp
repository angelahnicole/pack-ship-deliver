// Fill out your copyright notice in the Description page of Project Settings.

#include "PackShipDeliver.h"
#include "WoodCrate.h"


// Sets default values
AWoodCrate::AWoodCrate()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Create the static mesh component
	CrateMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CrateMesh"));
	RootComponent = CrateMesh;

	// Make it movable so we can edit the color
	CrateMesh->SetMobility(EComponentMobility::Movable);

	// Find the crate mesh and use that as the main mesh
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshFinder(TEXT("StaticMesh'/Game/Meshes/SM_Wood-Crate'"));
	if (MeshFinder.Succeeded()) CrateMesh->SetStaticMesh(MeshFinder.Object);

	DynamicMaterial = CrateMesh->CreateAndSetMaterialInstanceDynamic(0);
	DynamicMaterial->SetVectorParameterValue(FName("Crate_Color"), CrateColor);

}

// Called when the game starts or when spawned
void AWoodCrate::BeginPlay()
{
	Super::BeginPlay();
}