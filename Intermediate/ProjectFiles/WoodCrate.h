// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "WoodCrate.generated.h"

UCLASS()
class PACKSHIPDELIVER_API AWoodCrate : public AActor
{
	GENERATED_BODY()

public:
	// ----------------------------------------------------------------------------------------
	// Methods
	// ----------------------------------------------------------------------------------------

	// Sets default values for this actor's properties
	AWoodCrate();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Getter for the CrateMesh
	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return CrateMesh; }

	// ----------------------------------------------------------------------------------------
	// Attributes
	// ----------------------------------------------------------------------------------------

	// Color of the crate
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Materials")
	FLinearColor CrateColor;

	// Dynamic material instance of the crate
	class UMaterialInstanceDynamic* DynamicMaterial;

private:
	// ----------------------------------------------------------------------------------------
	// Attributes
	// ----------------------------------------------------------------------------------------

	// Static mesh to represent the crate in the level
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Crate", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* CrateMesh;
};
