// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "PackShipDeliver.h"
#include "PackShipDeliverGameMode.h"
#include "PackShipDeliverHUD.h"
#include "PackShipDeliverCharacter.h"
#include "OrderGameState.h"

APackShipDeliverGameMode::APackShipDeliverGameMode() : Super()
{
	// Set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// Use our custom game state class
	GameStateClass = AOrderGameState::StaticClass();

	// Use our custom HUD class
	HUDClass = APackShipDeliverHUD::StaticClass();
}
