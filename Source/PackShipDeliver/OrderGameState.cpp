

/////////////////////////////////////////////////////////////////////////////////////////////////

#include "PackShipDeliver.h"
#include "OrderGameState.h"

/////////////////////////////////////////////////////////////////////////////////////////////////

// =============================================================================================
// CONSTRUCTOR
// =============================================================================================
AOrderGameState::AOrderGameState() : Super()
{
	// Initialize game state
	CurrentOrderEnum = EOrderStateEnum::OSE_NoOrders;

	// Game scoring initialization
	OrderCostPercent = 0.05f;
	CurrentTier = 1;
	TotalCash = 0.f;

	// Order specific initialization
	MinOrderProducts = 1;
	MaxOrderProducts = 3;
	OrderSpawnRate = 1;
	OrderFulfillmentTime = 60.f;
	OrdersCompleted = 0;
	NextTierOrdersCompleted = 1;
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// =============================================================================================
// LEVEL TIERS AND SCORING
// =============================================================================================

void AOrderGameState::IncreaseTier()
{
	// Increase spawn rate
	if (OrderSpawnRate > 1)
	{
		OrderSpawnRate--;
	}

	// Increase max order products
	if (MaxOrderProducts < 30)
	{
		MaxOrderProducts++;
	}

	// Increase order cost percent
	OrderCostPercent = OrderCostPercent + 0.05;

	// Increase tier
	CurrentTier++;

	// Increase number of orders needed
	NextTierOrdersCompleted += NextTierOrdersCompleted;
}

void AOrderGameState::ModifyCash(float cashAmount)
{
	TotalCash = TotalCash + cashAmount;
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// =============================================================================================
// ORDERS
// =============================================================================================

void AOrderGameState::StartOrder(TArray<uint8> &orderProductBytes, float totalCost)
{
	// Set game state
	CurrentOrderEnum = EOrderStateEnum::OSE_OrderFulfillment;

	// Update order specifics
	OrderProductBytes = orderProductBytes;
	TotalOrderCost = totalCost;

	// Fail order if it wasn't completed in time
	GetWorld()->GetTimerManager().SetTimer(OrderTimer, this, &AOrderGameState::FailOrder, OrderFulfillmentTime, false);
}

bool AOrderGameState::CompleteOrder(TArray<uint8> &submittedProductBytes)
{
	// Initialize success
	bool isSuccessful = true;

	if (CurrentOrderEnum == EOrderStateEnum::OSE_NoOrders)
	{
		isSuccessful = false;
	}
	else
	{
		// Set game state
		CurrentOrderEnum = EOrderStateEnum::OSE_NoOrders;

		// Clear timer
		GetWorld()->GetTimerManager().ClearTimer(OrderTimer);

		// Check if the order was completed properly
		for (int32 iProduct = 0; iProduct < OrderProductBytes.Num(); iProduct++)
		{
			if (!submittedProductBytes.Contains<uint8>(OrderProductBytes[iProduct]))
			{
				isSuccessful = false;
				break;
			}
		}
	}

	if (isSuccessful)
	{
		// Reward player
		ModifyCash(TotalOrderCost * OrderCostPercent);

		// Increase orders completed
		OrdersCompleted++;

		// Check if we need to increase the tier
		if (OrdersCompleted >= NextTierOrdersCompleted)
		{
			IncreaseTier();
		}
	}
	else
	{
		// Punish player
		FailOrder();
	}

	return isSuccessful;
}

void AOrderGameState::FailOrder()
{
	// Set game state
	CurrentOrderEnum = EOrderStateEnum::OSE_NoOrders;

	// Punish player
	ModifyCash(-(TotalOrderCost * OrderCostPercent));
}

bool AOrderGameState::IsOrderActive()
{
	return GetWorld()->GetTimerManager().IsTimerActive(OrderTimer);
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// =============================================================================================
// GETTERS AND SETTERS
// =============================================================================================

float AOrderGameState::GetOrderFulfillmentTime() { return  OrderFulfillmentTime; }

int32 AOrderGameState::GetTier() { return  CurrentTier; }

int32 AOrderGameState::GetCompletedOrders() { return  OrdersCompleted; }

float AOrderGameState::GetTotalCash() { return TotalCash; }

/////////////////////////////////////////////////////////////////////////////////////////////////


