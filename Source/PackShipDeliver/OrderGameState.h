// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameState.h"
#include "OrderGameState.generated.h"

UENUM(BlueprintType)
enum class EOrderStateEnum : uint8
{
	OSE_NoOrders UMETA(DisplayName = "No Orders"),
	OSE_OrderFulfillment UMETA(DisplayName = "Order Fulfillment")
};

/**
 * 
 */
UCLASS()
class PACKSHIPDELIVER_API AOrderGameState : public AGameState
{
	GENERATED_BODY()
	
public:
	AOrderGameState();

	/////////////////////////////////////////////////////////////////////////////////////////////////

	/**  */
	UFUNCTION(BlueprintCallable, Category = "GameState")
	void IncreaseTier();

	/**Adds or subtracts (depending on what you give it) from your TotalCash  */
	UFUNCTION(BlueprintCallable, Category = "GameState")
	void ModifyCash(float cashAmount);

	/////////////////////////////////////////////////////////////////////////////////////////////////

	/**  */
	UFUNCTION(BlueprintCallable, Category = "Orders")
	void StartOrder(UPARAM(ref) TArray<uint8> &orderProductBytes, float totalCost);

	/**  */
	UFUNCTION(BlueprintCallable, Category = "Orders")
	bool CompleteOrder(UPARAM(ref) TArray<uint8> &submittedProductBytes);

	UFUNCTION(BlueprintCallable, Category = "Orders")
	void FailOrder();

	/**  */
	UFUNCTION(BlueprintPure, Category = "Orders")
	bool IsOrderActive();

	/////////////////////////////////////////////////////////////////////////////////////////////////

	/** Getter for the order fulfillment time */
	UFUNCTION(BlueprintPure, Category = "Orders")
	float GetOrderFulfillmentTime();

	/** Getter for the tier */
	UFUNCTION(BlueprintPure, Category = "GameState")
	int32 GetTier();

	/** Getter for the successfully completed orders  */
	UFUNCTION(BlueprintPure, Category = "Orders")
	int32 GetCompletedOrders();

	/** Getter for the player's total cash  */
	UFUNCTION(BlueprintPure, Category = "GameState")
	float GetTotalCash();

	/////////////////////////////////////////////////////////////////////////////////////////////////

	/**  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Orders")
	EOrderStateEnum CurrentOrderEnum;

	/**  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Orders")
	FTimerHandle OrderTimer;

	/**  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Orders")
	float OrderSpawnRate;

	/**  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Orders")
	int32 MaxOrderProducts;

	/**  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Orders")
	int32 MinOrderProducts;

	/////////////////////////////////////////////////////////////////////////////////////////////////

private:
	/** The time that is given to you to complete the order  */
	float OrderFulfillmentTime;

	/** The percent of the total cost that is rewarded to you for completing an order  */
	float OrderCostPercent;

	/** The total amount of money that has been rewarded to the player  */
	float TotalCash;

	/**  The difficulty of the game. The higher the tier, the harder it is, but the more $$$ you get */
	int32 CurrentTier;

	/** The number of orders that have been successfully completed */
	int32 OrdersCompleted;

	/* The number of orders needed to complete in order to advance another tier */
	int32 NextTierOrdersCompleted;

	/////////////////////////////////////////////////////////////////////////////////////////////////

	/** Array of integers that represent product enums of the current order  */
	TArray<uint8> OrderProductBytes;

	/** Cost of the current order  */
	float TotalOrderCost;

	/////////////////////////////////////////////////////////////////////////////////////////////////
};
