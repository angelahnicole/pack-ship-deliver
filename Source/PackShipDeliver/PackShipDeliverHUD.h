// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"
#include "PackShipDeliverHUD.generated.h"

UCLASS()
class APackShipDeliverHUD : public AHUD
{
	GENERATED_BODY()

public:
	APackShipDeliverHUD();

	/** Whether or not to draw the crosshair */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
	bool bDrawCrosshair;

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

