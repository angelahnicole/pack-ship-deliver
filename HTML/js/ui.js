// ================================================================================================================================
// ui.js
// -------------------------------------------------------------------------------------------------------------------------------
// @author Angela Gross
// Advanced Client-Side Web Programming
// -------------------------------------------------------------------------------------------------------------------------------
// Methods used for the ui components for the game
// ================================================================================================================================

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var inventoryLIFormat = '<li id="{3}"><span id="volume">{0}</span> <small>units cubed</small> <span id="name">{1}</span> <span id="type" class="pull-right">{2}</span></li>';
var inventoryIFormat = '<span id="volume">{0}</span> <small>units cubed</small> <span id="name">{1}</span> <span id="type" class="pull-right">{2}</span>';
var totalVolumeFormat = '<span id="volume">{0}</span> <small>units cubed</small>';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// SCORING

function updateTier(tier)
{
    // Zero pad tier value
    var stringifiedValue = tier.toString();
    var paddedValue = stringifiedValue.leftPad("0", 4);
    
    // Put into element
    $('#tier').html(paddedValue);
}

function updateTime(seconds)
{
    // Zero pad time value
    var stringifiedValue = seconds.toString();
    var paddedValue = stringifiedValue.leftPad("0", 4);
    
    // Put into element
    $('#time').html(paddedValue);
}

function updateCash(cash)
{
    // Zero pad cash value
    var stringifiedValue = cash.toString();
    var paddedValue = stringifiedValue.leftPad("0", 10);
    
    // Put into element
    $('#cash').html(paddedValue);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// INVENTORY

function addItem(volume, name, type, id, listID, listLIFormat, listIFormat)
{
    listID = listID || "ul.inventory-list";
    listLIFormat = listLIFormat || inventoryLIFormat;
    listIFormat = listIFormat || inventoryIFormat;
    
    // Zero pad volume value
    var stringifiedValue = volume.toString();
    var paddedValue = stringifiedValue.leftPad("0", 3);

    if($("#" + id).length > 0)
    {
        // Put in item details into HTML
        var formattedHTML = listIFormat.format(paddedValue, name, type);
        
        // Modify existing item
        $("#" + id).html(formattedHTML);
    }
    else
    {
        // Put in item details into HTML
        var formattedHTML = listLIFormat.format(paddedValue, name, type, id);
        
        // Add to inventory list
        $(listID).append(formattedHTML);
    }
}

function removeItem(id)
{
    if($("#" + id).length > 0)
    {
        // Remove item
        $("#" + id).remove();
    }
}

function emptyInventory()
{
    $("ul.inventory-list").html("");
    
    updateTotalVolume(0);
}

function updateTotalVolume(total)
{
    // Zero pad cash value
    var stringifiedValue = total.toString();
    var paddedValue = stringifiedValue.leftPad("0", 3);
    
    // Put in volume details into HTML
    var formattedHTML = totalVolumeFormat.format(paddedValue);
    
    // Put into element
    $('#total-volume').html(formattedHTML);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ORDER

function populateOrder(id, name, address, products, shippingCost, totalCost)
{
    // Zero pad and add order id
    var stringifiedValue = id.toString();
    var paddedValue = stringifiedValue.leftPad("0", 5);
    $('#order-id').html(paddedValue);
    
    // Add name
    $('#customer-name').html(name);
    
    // Add address
    $('#customer-address').html(address);
    
    // Clear products
    $("ul.products-list").html("");
    
    // Add products
    $.each(products, function(key, product)
    {
       addItem(product["volume"], product["name"], "", product["id"], "ul.products-list"); 
    });
    
    // Zero pad and add shipping cost
    stringifiedValue = shippingCost.toString();
    paddedValue = stringifiedValue.leftPad("0", 5);
    $('#shipping-cost').html(paddedValue);
    
    // Zero pad and add total cost
    stringifiedValue = totalCost.toString();
    paddedValue = stringifiedValue.leftPad("0", 5);
    $('#total-cost').html(paddedValue);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// INTERACTION

function updateInteraction(key, actionText)
{
    // Add key
    $('#interact-key').html(key);
    
    // Add action text
    $('#interact-action').html(actionText);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function populateBoxInventory(items)
{
    $.each(items, function(key, item)
    {
        addItem(item["volume"], item["name"], "", item["id"], "#box-packing-list"); 
    });
}

function emptyBoxInventory()
{
    $("#box-packing-list").html("");
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////