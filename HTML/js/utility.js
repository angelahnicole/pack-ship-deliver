// ================================================================================================================================
// utility.js
// -------------------------------------------------------------------------------------------------------------------------------
// @author Angela Gross
// Advanced Client-Side Web Programming
// -------------------------------------------------------------------------------------------------------------------------------
// Utility javascript methods
// ================================================================================================================================

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ===========================================================================================================================
// FORMAT
// --------------------------------------------------------------------------------------------------------------------------
// Formats a string with the given arguments.
// ===========================================================================================================================
if (!String.prototype.format) 
{
  String.prototype.format = function() 
  {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) 
    { 
      return typeof args[number] !== 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

// ===========================================================================================================================
// LEFT PAD
// --------------------------------------------------------------------------------------------------------------------------
// Adds values to the left of a string
// ===========================================================================================================================
if(!String.prototype.leftPad)
{
    String.prototype.leftPad = function(padString, length) 
    {
        var str = this;
        
        while (str.length < length)
        {
            str = padString + str;
        }
        
        return str;
    };
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////